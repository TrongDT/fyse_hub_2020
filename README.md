# Welcome to FYSE hub
> The second-best hub out there!
## Description
This repository will contain various code we have written together and other useful resources. Please notify me if there's something missing from this page that you want me to add, or you can send me a merge-request!

## Lectures
##### IKT111:
- [Lecture - DFS](https://youtu.be/ZkrCc-ab1lg)
- [Lecture - Recursion](https://youtu.be/0TDqLadwdGw)
- [Lecture - Snake v2](https://youtu.be/ZTq0IdT6o6I)	

##### IKT101:
- [Lecture - IKT101 - Assignment_3](https://youtu.be/Jd1sEYduvWQ)
- [Lecture - IKT101 - Assignment_4](https://youtu.be/BZT0ErAFQ6E)
- [Lecture - IKT101 - Assignment_5](https://youtu.be/W8gAK7GsnLc)
- [Lecture - IKT101 - Assignment_6](https://youtu.be/81Uo3Wrx2Ow)

## Coding Practice and Fun
In case you were not struggling enough with your current coding assignments:
- [Kattis](https://open.kattis.com/): Fun coding challenges from actual coding compeditions (Remember to join our [University](https://open.kattis.com/universities/uia.no))

- [Advent of Code](https://adventofcode.com/): Annual set of coding challenges (Join our FYSE leaderboard `665622-85fb1ee8`!)

- [Project Euler](https://projecteuler.net/): Problems for nerds that like math.

- [Codewars](https://www.codewars.com/): Slick looking coding challenge website!

- [Git Tutorial](https://learngitbranching.js.org/): Interactive Git Tutorial, highly recommended!

## YouTube channels to watch

- [Code Bullet](https://www.youtube.com/channel/UC0e3QhIYukixgh5VVpKHH9Q): From the Code Bullet himself: "Just an idiot with a computer science degree trying his best"

- [Primer](https://www.youtube.com/channel/UCKzJFdi57J53Vr_BkTfN3uQ): Well-made videos about academic subjects but shown with cute blobs. Highly recommended!

- [Computerphile](https://www.youtube.com/user/Computerphile): Videos all about computers and computer stuff.

- [AI_ROLL](https://www.youtube.com/watch?v=dQw4w9WgXcQ): Perhaps my favorite AI channel, definitely one of the greatest out there.



## Recommended Podcasts
Stay awhile and listen!

[![Darknet Diaries](https://is1-ssl.mzstatic.com/image/thumb/Podcasts123/v4/63/8f/60/638f60d7-a09b-ab94-20d2-fa6e651c6ff0/mza_1582559084056090833.jpg/150x150bb.jpg "Darknet Diaries Spotify Link")](https://open.spotify.com/show/4XPl3uEEL9hvqMkoZrzbx5?si=F2vg0y6qSV2Xpz8JV4e3Ug) [![Unsupervised Learning Picture](https://is4-ssl.mzstatic.com/image/thumb/Podcasts114/v4/67/6b/d5/676bd536-d70e-4693-7e62-9f2dc1ef2d75/mza_14141221886166756843.jpg/150x150bb.jpg "Unsupervised Learning Spotify Link")](https://open.spotify.com/show/0cIzWAEYacLz7Ag1n1YhUJ?si=VCNcCe3cRMCFkLxDAYbXQQ)  [![Reply All Picture](https://is2-ssl.mzstatic.com/image/thumb/Podcasts114/v4/30/fe/c5/30fec595-c772-f80e-289b-4974844224b4/mza_4773085836270850850.jpg/150x150bb.jpg "Unsupervised Learning Spotify Link")](https://open.spotify.com/show/7gozmLqbcbr6PScMjc0Zl4?si=3lClSlpGTjCACLkfEnAtIA)  [![Game Over Picture](https://is5-ssl.mzstatic.com/image/thumb/Podcasts113/v4/f7/f6/ef/f7f6efe1-4dda-5e22-39ae-a0fb4f3f0ac5/mza_1519634673412818881.jpg/150x150bb.jpg "Unsupervised Learning Spotify Link")](https://open.spotify.com/show/5MVDG1MO0sNV3Td7sjog7U?si=d7SiBFS-QHqdp-Rf11whxg)
[![Programming Throwdown](https://is1-ssl.mzstatic.com/image/thumb/Podcasts113/v4/f0/8c/76/f08c7677-1e2c-7b08-75f9-841ecda0bafd/mza_1048328501313038349.png/150x150bb.jpg "Programming Throwdown")](https://open.spotify.com/show/274Z0vXSCYxddYGj2hLJ8r?si=G-OdbkESRdeX1N585QVKjg)

If you know some other relevant podcasts that fits this list, do share!