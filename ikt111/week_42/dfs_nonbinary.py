class Node:
    def __init__(self, name):
        self.name = name
        self.children = []

    def add_child(self, node):
        self.children.append(node)

    def __repr__(self):
        return self.name


n1  = Node('A')
n2  = Node('B')
n3  = Node('C')
n4  = Node('D')
n5  = Node('E')
n6  = Node('F')
n7  = Node('G')
n8  = Node('X')
n9  = Node('Y')
n10 = Node('Z')
n11 = Node('?')


n1.add_child(n2)
n1.add_child(Node('!'))
n1.add_child(n3)

n2.add_child(n4)
n2.add_child(n5)

n3.add_child(n6)
n3.add_child(n7)
n3.add_child(n11)

n7.add_child(n8)
n7.add_child(n9)
n7.add_child(n10)



# Prints out the binary tree in a dfs-manner
def dfs(root):

    if root is None:
        return
    
    # Initial values
    node_stack = []  # Pretend this is a stack
    node_stack.append(root)

    # Loop until node_stack is empty
    while len(node_stack) != 0:

        current_node = node_stack.pop()

        for child in reversed(current_node.children):
            node_stack.append(child)

        print(current_node)
    

dfs(n1)

