class Node:
    def __init__(self, name):
        self.name = name
        self.left = None
        self.right = None

    def __repr__(self):
        return self.name



n1 = Node('A')
n2 = Node('B')
n3 = Node('C')
n4 = Node('D')
n5 = Node('E')
n6 = Node('F')
n7 = Node('G')


n1.left = n2
n1.right = n3

n2.left = n4
n2.right = n5

n3.left = n6
n3.right = n7


# a = node_stack.pop()

# a = node_stack[-1]
# del node_stack[-1]

# Prints out the binary tree in a dfs-manner
def dfs(root):

    if root is None:
        return
    
    # Initial values
    node_stack = []  # Pretend this is a stack
    node_stack.append(root)

    # Loop until node_stack is empty
    while len(node_stack) != 0:

        current_node = node_stack.pop()

        if current_node.right is not None:
            node_stack.append(current_node.right)

        if current_node.left is not None:
            node_stack.append(current_node.left)

        print(current_node)
    

dfs(n1)

