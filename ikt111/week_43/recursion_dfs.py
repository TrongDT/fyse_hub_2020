class Node:
    def __init__(self, name):
        self.name = name
        self.children = []

    def add_child(self, node):
        self.children.append(node)

    def __repr__(self):
        return self.name


A = Node('A')
B = Node('B')
C = Node('C')
D = Node('D')
E = Node('E')
F = Node('F')
G = Node('G')
X = Node('X')
Y = Node('Y')
Z = Node('Z')

A.add_child(B)
A.add_child(C)

B.add_child(D)
B.add_child(E)

C.add_child(F)
C.add_child(G)

G.add_child(X)
G.add_child(Y)
G.add_child(Z)


# Case 3
S = Node('S')
T = Node('T')
R = Node('R')

S.add_child(T)
S.add_child(R)


def recursive_dfs(node):

    if len(node.children) == 0:  
        print(node)  # Print myself
    else:
        print(node)  # Print myself
        for n in node.children:
            recursive_dfs(n)

recursive_dfs(A)
