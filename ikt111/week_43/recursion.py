# Recursion: A method that calls itself.

# Factorial
# 6! = 6 * 5 * 4 * 3 * 2 * 1 = 720
# n! = n * (n-1) * (n-2) * ... * 1 

def factorial(n):
    # Base Case: Cases where the result given an input is super easy to calculate (i.e. no recurring)
    # 1! = 1
    # 0! = 1 
    if n <= 1:    # Base Case
        return 1
    else:    
        return n * factorial(n-1)


def factorial_iterative(n):
    result = 1
    for i in range(n):
        result *= (i + 1)
    return result    

# Recursion way
print(factorial(0))
print(factorial(1))
print(factorial(2))
print(factorial(3))
print(factorial(4))
print(factorial(5))
print(factorial(6))

print()

# Iterative way
print(factorial_iterative(0))
print(factorial_iterative(1))
print(factorial_iterative(2))
print(factorial_iterative(3))
print(factorial_iterative(4))
print(factorial_iterative(5))
print(factorial_iterative(6))
print(factorial_iterative(1000))

