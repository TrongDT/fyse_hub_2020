import random
from snake import SnakeGame

# Class = Recipe
class Node:
    def __init__(self, position, move, parent):
        self.position = position
        self.move = move
        self.parent = parent

    def get_path(self): # = moves_list()
        path = []
        current_node = self
        while current_node.parent != None:  # Loop until I reach root node
            path.append(current_node.move)
            current_node = current_node.parent
        path.reverse() 
        return path

snake = SnakeGame()


def bfs():
    """ 
    Find path to apple using BFS algorithm. 
    """

    # Initial values
    head_pos = snake.get_snake_head_position() # [x, y]
    root = Node(head_pos, None, None)
    nodes = [root]  # Relevant nodes to check
    visited = []    # List over already visited coordinates # Hint: Use 'set' for better performance
    
    while len(nodes) != 0:

        current_node = nodes.pop(0) # Retrieve first element, and delete from list
        current_path = current_node.get_path()

        # Check if current node reside on top of the apple
        if snake.is_winning(current_path):
            return current_path
        

        # Check available paths
        for move in ['left', 'down', 'right', 'up']:
            
            new_pos = snake.simulate_move(current_node.position, move) # [x,y]
            
            # If the current position has already been explored, ignore this path
            if new_pos in visited:
                continue

            new_path = current_node.get_path() 
            new_path.append(move)

            # Check if current path is legal
            if not snake.is_legal(new_path):   
                continue
                    
            # Create Child
            child = Node(new_pos, move, current_node)
            nodes.append(child)
            visited.append(child.position)
            
    return  # suicide


@snake.register_ai
def super_ai(): # Plan out path to apple, then return path
    return bfs()
   

snake.start(use_ai=True)
