# Assignment_5_x
### Prerequisites
- `scanf()` / `printf()`
- Array
- Loop
- Functions
- String
- Structures
- File IO



## FAQ

The following chapters explain some neat tricks that may come in handy for all assignments within assignment 5.

### What is a structure?

A **structure** in C a data type that can contain **multiple data items**. A structure in C is created through the `struct` statement, as seen below:

###### Coordinate Example
A structure that contains two integers, `x` and `y`.
```c
struct Coordinate{
	int x;
	int y;
};
```


###### Student Example

A structure that represents the information a student will have. In this example, a student ID, a name, and an age.
```c
struct Student{
	int id;
	char name[30];
	int age;
};
```



### Arrays vs Structures?

##### Arrays
Recall that an **array** is used to store **multiple values** within the **same** data type. E.g. an `int` array can **only** contain values of type `int`. To access the values inside an array one has to use **indices**:

```c
int numbers[] = {4, 6, 7, 1, 5};  // Array creation
printf("%i", numbers[2]);         // Accessing an element at index 2 => int: 7
```

##### Structures
A **structure** is used to store **multiple variables** that do **not** need to be of the same data type. Variables of a structure are also called **members**. Furthermore, you don't access its members by indices like arrays, but by the members' name:

```c
struct Student{
	int id;			// member of type 'int' named 'id'
	char name[30];  // member of type 'char array' named 'name'
	int age;		// member of type 'int' named 'age'
};

struct Student s;   // Student declaration named 's'

strcpy(s.name, "Peter");  	// Assign name using ".name" (and NOT s[0] like arrays!)
s.age = 20;					// Assign age using ".age"   (and NOT s[1] like arrays!)
```



### How to actually use a structure?

I like to think of structures of **custom-made data types** (i.e. existing data types being `int`, `char`, `float`, etc.).

To use a structure, you have to do two things:
1. **Define** the structure.
2. **Declaring** an instance of said structure.

Or, if you compare the above steps where *a structure is a cake* instead:
1. **Write** the recipe for a cake.
2. **Bake** a cake using said recipe.

Although simplified, the cake example above introduces an **important idea**: *The recipe for a cake is **useless** unless some cakes are baked using it!*

Or, when we are talking about structures: *A structure definition is **useless** until some declarations are created using it.*

##### Defining a structure

Defining a structure is about defining **members** of a structure:
```c
// Structure definition - A structure named 'Student'
struct Student{
	int id;			// member of type 'int' named 'id'
	char name[30];  // member of type 'char array' named 'name'
	int age;		// member of type 'int' named 'age'
};
```

This structure definition is useless before you actually use it. 

##### Declaring a structure

Just like you can declare `int` variables:
```c
int a;  // Declaration of 'int' variable named 'a'
int b;	// Declaration of 'int' variable named 'b'

a = 5;	// Assign values to 'a'
b = 10; // Assign values to 'b'
```

You can now declare `struct Student` variables from the same defintion `Student`:
```c
// Student declaration
struct Student student_0; // declaration of type 'Student' named 'student_0'
struct Student student_1; // declaration of type 'Student' named 'student_1'

// Assign values to Student student_0
strcpy(student_0.name, "Peter");  	// Assign name
student_0.age = 20;					// Assign age
```

**Tip:** My way of remembering the above is to label the statements as seen in the picture below. Simplified, defining a structure is basically creating your own data types!
![](https://i.imgur.com/QCeiQ7M.png)


### An example of multiple declaration of the same structure

In the code below, `student_0`, `student_1` and `student_2` are three **different variables** of the **same** structure `struct Student`:
```c
// Structure definition: A structure named 'Student'
struct Student{
	int id;			// member of type 'int' named 'id'
	char name[30];  // member of type 'char array' named 'name'
	int age;		// member of type 'int' named 'age'
};

// Student declaration
struct Student student_0; // declaration of type 'Student' named 'student_0'
struct Student student_1; // declaration of type 'Student' named 'student_1'
struct Student student_2; // declaration of type 'Student' named 'student_1'


// Assign values to Student student_0
strcpy(student_0.name, "Peter");  	// Assign name
student_0.age = 20;					// Assign age

// Assign values to Student student_1
strcpy(student_1.name, "Lisa");  	// Assign name
student_1.age = 10;					// Assign age

// Assign values to Student student_2
strcpy(student_2.name, "John");  	// Assign name
student_2.age = 12;					// Assign age
```


### Creating functions around structures?
As structures behave as a data type, you can make cool functions with it:


##### Structures used as parameters

```c
#include <stdio.h>
#include <stdbool.h>

// Our cool struct containing only one member
struct Ninja{
    bool alive;
};

// Our function
void check_health(struct Ninja nin){
    if(nin.alive == true){
        printf("Ninja is alive!");
    } else{
        printf("Ninja is dead!");
    }
}


int main(){

    // Declare a ninja!
    struct Ninja n;
    n.alive = true;

    check_health(n); // Call function

    return 0;
}
```


##### Return type is a structure

```c
// Our cool struct containing only one member
struct Ninja{
	char name[30];  
};

// A function that returns a value of type 'struct Ninja'
struct Ninja create_decoy(){
	struct Ninja temp_ninja;
	strcpy(temp_ninja.name, "Wood");
	return temp_ninja;
}


int main(){

	// Calling the above functions
	struct Ninja ninja_0 = create_decoy();
	struct Ninja ninja_1 = create_decoy();
	struct Ninja ninja_2 = create_decoy();
	
    return 0;
}
```

### What is the deal with `typedef`?

The original use-case for `typedef` is for **aliasing**, or in simpler words: creating a **nickname** for something.

```c
typedef int poop;  // statement 'poop' can now be used in-place of 'int'

poop number = 5;   // Ain't coding fun?
```

A good use-case for `typedef` is when used to "shorten" down a `struct` for convenience. 

##### Without `typedef`

```c
struct Student{
	int id;
	char name[30];
	int age;		
};

struct Student s;
```

##### With `typedef`

```c
typedef struct Student{
	int id;
	char name[30];
	int age;		
} student_t;

student_t s;  // <-- Do you prefer this over 'struct Student s'?
```

In the above code snippet, the name `student_t` is arbitrary, you can choose something different if you want.

**NOTE:** Using `typedef` is **optional**!

**OBSERVATION:** I have seen multiple students from earlier years fail to understand how structures work because they think `typedef` is mandatory for structures! 😰



## Assignment_5_1

**Tip:** To create an alias requires the usage of `typedef`.

**Tip:** The student id, although numerical, may be **too big** for a numerical data types. Perhaps read it in as a string and opt for **char array** instead?

**Tip:** The above tips in the FAQ should be plenty enough to solve this assignment!



## Assignment_5_2

This assignment may be difficult for some of you, as it can be rather demanding. Perhaps the following tips can help you!

### Setting your project as working directory

As you will be working with files in this assignment, you should set your project as the working directory:

![](https://i.imgur.com/bJGNwV4.png)



### Create .txt files for testing

Although not mandatory, I recommend you to create `student_read.txt` and `student_write.txt` as seen below.

![](https://i.imgur.com/E1PUMTG.png)

Manually populate the `student_read.txt` with information alike the following:

![](https://i.imgur.com/leDIU8Z.png)

You can test your application against these files however you want. No need to be careful; the Bamboo test has its own version of these files.



### Creating the menu

Creating the "application menu" is the first hurdle in this assignment. The following snippet can be used as an inspiration:

```c
int main(){
	
	// Loop forever
    while (1){

        int choice = 0;

        scanf("%i", &choice);

        if(choice == 1){
            // Do choice number 1
        }
        else if (choice == 2){
            // Do choice number 2
        }
        else{
            // Break loop
            break;
        }

    }

    return 0;
}
```

**Tip:** You can create something similar to the above code snippet with `switch` statement.


### Menu option 1 - Reading

For menu option 1, something along the following setup can be used:

```
1. Open file "student_read.txt"
2. Declare new student
3. Read from file:
    3.2. Store id in declared student
    3.3. Store name in declared student
    3.4. Store age in declared student
4. Print values:
    4.1. print stored id
    4.2. print stored name
    4.3. print stored age
5. Close file "student_read.txt"
```

**Tip:** When storing the data, I can recommend you to use the structure `student_t` you created in assignment_5_1.

When reading from file, the following functions can be used:
- `fgets()`: Read string from file.
- `fscanf()`: Read string from file and format it.

**Tip:** It is the same `fgets()` as the one you used in assignment_4! 


### Menu option 2 - Writing

For menu option 2, something along the following setup can be used:

```
1. Open file "student_write.txt"
2. Declare new student
3. Get user input:
    3.1. Store id in declared student
    3.2. Store name in declared student
    3.3. Store age in declared student
4. Write to file:
    4.1. write stored id
    4.2. write stored name
    4.3. write stored age
5. Close file "student_write.txt"
```

When writing to file, I can recommend you the following functions:
- `fputs()`: Writes string to file.
- `fprintf()`: Write **formatted** string to file.

**Tip:** `fprintf()` is almost the same as `printf()`. `printf()` prints to console by default, whereas with `fprintf()` you can determine **output destination**. In short: if you know how to use `printf()`, then `fprintf()` is a good choice for you!