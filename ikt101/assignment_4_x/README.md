# Assignment_4_x
### Prerequisites
- `scanf()` / `printf()`
- Array
- Loop
- Functions
- String
	- `fgets()`
	- `strlen()`
	- `strstr()`
	- `strcmp()`
	- `strcpy()`

## General Tips for Assignment 4

The following chapters explain some neat tricks that may come in handy for all assignments within assignment 4.

### What is a "character"?

A **character** in C is a **8 bit = 1 byte** datatype (whereas an integer is a **4 byte** datatype). 

The **ASCII table** below defines all characters and their corresponding decimal value `Dec`. 
Noteworthy characters:
- dec = `0` is the NULL character `\0`
- dec = `10` is the newline character `\n`
- dec = `65 - 90` defines the upper-case characters.
- dec = `97 - 122` defines the lower-case characters.


[![Table](https://jbwyatt.com/I/asciiGood.gif "ASCII Table link")](http://www.asciitable.com/) 


### The relationship between characters and integers

Try to digest the following: *All datatypes are in reality just bits and bytes*.

Consider the following example:

We have a byte containing the bits `01000101`. 
- If we were to ask the computer to output `01000101` in `int`-format, we would get the value `69`.
- However, if we were to ask the computer to output `01000101` in `char`-format instead, we would get the value `E`.

This can be be proven with the following code:
```c
int binary = 0b01000101; // Begin with '0b' to write in binary instead of decimal.
printf("%i\n", binary);  // Outputs '69'
printf("%c\n", binary);  // Outputs 'E'
```

**In other words:** `int` and `char` can be **converted** back-and-forth.

Why is this important to understand? You can do cool stuff with this knowledge, and some may even come in handy when you are doing your assignments! 

##### Examples:
```c
// You can store a 'character' in `int`
int a = 'c';
printf("%i", a);

// Output: 99
```

```c
// You can store an 'int' in `char`
char b = 68;
printf("%c", b);

// Output: D
```

```c
// Prints the english alphabet!
for(char i = 'a'; i <= 'z'; i++){
    printf("%c ", i);
}

// Output: a b c d e f g h i j k l m n o p q r s t u v w x y z
```

### What is a "string"?

In programming, a **string** is a **sequence of characters**. 

In C, a **string** is a **character array**.

```c
string my_string;   // <-- Eww no
char my_string[10]; // <-- This is a string = character array!
```



### When to use `scanf()` vs `fgets()`
Recall that `scanf()` does not include **whitespaces** (e.g. spaces, newlines `\n`, etc.) in the input. `fgets()` on the other hand does. A rule of thumb on when to use `scanf()` versus `fgets` is:
- Use `scanf()` if you expect the input to be **only one word**.
- Use `fgets()` if you expect the input to be **multiple words / sentences**.


### The dark side of 'fgets()'
Consider the following code snippet:

```c
char name[10];
printf("Please type your name:\n")
fgets(name, 10, stdin);
```

We execute it and type in `Peter`:
```
Please type your name:
> Peter
```

Can you guess the content of `name`? If you guessed `P`, `e`, `t`, `e`, `r`, you are partly correct. In reality, the following is true:

|`name`| `P` | `e` | `t` | `e` | `r` |`\n` |`\0` | ... |
|:----:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|index |  0  |  1  |  2  |  3  |  4  |  5  |  6  | ... |

Oh no, we have the character `\n` in index `5`! 

In hindsight, it is understandable why this is the case. When we typed `Peter` earlier, we ended the prompt by pressing the `Enter`-key, which types the `\n`. Furthermore, `fgets()` includes **all** whitespaces in the input, including said `\n`.

This trailing `\n` is rather problematic. Consider this scenario:
```c
char name[10];
printf("Please type your name:\n")
fgets(name, 10, stdin);

length = strlen(name); // Calculates the length of `name`
print("%i", length);   // Prints the result from previous line
```

This code will print out `6` despite `Peter` being `5` characters long. `strlen()` counts all characters until it encounters the **first** `\0`, including the trailing `\n`.

In most cases, you are better off removing the `\n`.


### How to remove a trailing `\n`

Lucky for us, removing a trailing `\n` is very trivial.

Adding ``name[strlen(name) - 1] = `\0`;`` after `fgets()` does the trick. Using the above example:

```c
char name[10];
printf("Please type your name:\n")
fgets(name, 10, stdin);

name[strlen(name) - 1] = `\0`; // <-- This does the trick!

length = strlen(name); // Calculates the length of `name`
print("%i", length);   // Prints the result from previous line
```


## Assignment_4_1
I will keep this one brief, where you have to find out the rest yourself!

### Tasks


#### Task 1 - Write an application that asks the user for a word

Use what you have learned:
```c
char word[50];
printf("Please type your word:\n")
fgets(name, 50, stdin);
word[strlen(word) - 1] = `\0`;
```


#### Task 1 - Tells how many letters are in the word

**Tip**: Use `strlen()`


#### Task 2 - Prints the word reversed

This one can be tricky if you are still just dabbling in C.

Consider this method:

We want to reverse the word `h e l l o`. A way to do this is by first swapping the first and the last elements with each other, then the second with second-last, then the third with the third-last, etc. Repeat this step until you reach the middle of the word.

##### Figuratively

**Start:** `h e l l o`

1. `[h] e l l [o]` -> `[o] e l l [h]` : Swap element 0 with element 4
2. `o [e] l [l] h` -> `o [l] l [e] h` : Swap element 1 with element 3
3. `o l [l] e h`   -> `o l [l] e h`: Swap element 2 with element 2 (Unnecessary, but does not hurt to do!)

**Result:** `o l l e h`

##### In code:

```c
char word[] = {"hello"};  // Array is automatically initialized with 6 elements.

for (int i = 0; i < strlen(word) / 2; ++i) {
    char backup = word[i];
    word[i] = word[strlen(word) - 1 - i];
    word[strlen(word) - 1 - i] = backup;
}

printf("%s", word);  // Outputs: 'olleh'
```


**Tip:** This is is similar to how we swapped two elements of an array during bubble sort in `assignment_3_1/README.md`!

**Tip:** Use the logic above in a function as it is required.

#### Task 3 - Tells if the word is a palindrome

**Tip**: `strcmp()` is pretty useful here.

**Tip**: The reverse logic from previous task may come in handy here.


## Assignment_4_2

#### Task 1 - Prints the string in uppercase and lowercase
**Tip**: `tolower()` and `toupper()` is your friends here.


#### Task 2 - Splits the string on the middle and prints the two parts with " - " between
**Note**: The string length is always even in this task.

**Tip**: See if you can manage to solve it with the following pseudocode only.
```c
// PSEUDOCODE
char word[] = {"Alpaca"};

printf("Alp"); // Print only left part somehow

printf(" - ");

prinft("aca"); // Print only right part somehow

```



## Assignment_4_3
**Tip**: You should declare **two char arrays** in this assignment.


#### Task 1 - Tells if the strings are equal or not

**Tip**: `strcmp()` is pretty useful here.

#### Task 2 - Tells if one string is a substring of the other

**Tip**: `strstr()` is pretty useful here.

**Tip**: Here are some pseudocode:

```c
// PSEUDOCODE
char word1[] = {"Alpaca"};
char word2[] = {"Academy"};

if word1 is substring of word2 OR word2 is substring of word1:
    print "It is a substring!"
else
    print "It is not a substring!"
```


## Assignment_4_4

This one is rather difficult, so I will give you some more tips here.

##### Problem 1: WHERE do we store our info?
You should have a way to store number of times each letter has been counted. A great way to do this is by using an **int array** that can contain 26 integers. Initialize all 26 integers into value `0`.

```c
int counts[26] = {0};
```

In other words, you will have this:

|`counts`| `0` | `0` | `0` | `0` | `0` | `0` | `0` | ... |
|:------:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| index  |  0  |  1  |  2  |  3  |  4  |  5  |  6  | ... |


##### Problem 2: HOW do we store our info?

You know how you can store value in a specific index when it comes to array like so:

```c
int counts[26] = {0};

counts[2] = 1;  // value at index 2 is now 1.
counts[5] = 6;  // value at index 5 is now 6.
counts[9] = 4;  // value at index 9 is now 4.
```

Wouldn't it be very cool if you could index by characters instead of numbers, like this:
```c
int counts[26] = {0};

counts['a'] = 1;  // ERROR: Out of boundaries! index 'a' = 97
counts['b'] = 6;  // ERROR: Out of boundaries! index 'b' = 98
counts['c'] = 4;  // ERROR: Out of boundaries! index 'c' = 99
```

If this worked, it would be very handy for this very assignment. You can actually do something very similar to this. From what we know about the relationship between integers and characters, we know that the following is true:

```c
'a' - 'a' = 0
'b' - 'a' = 1
'c' - 'a' = 2
```

With this knowledge, we can fix the above errors:
```c
int counts[26] = {0};

counts['a' - 'a'] = 1;  // Value at index 0 = 97
counts['b' - 'a'] = 6;  // Value at index 1 = 98
counts['c' - 'a'] = 4;  // Value at index 2 = 99
```

See how you can use this in your assignment? 😉

##### Problem 3: How do we count the number of occurrences of a letter?

The following code counts the number of occurrences of `a` in the string `alpaca`.
```c
char word[] = {"alpaca"};

int a = 0;

for(int i = 0; i < strlen(word); i++){
    if(word[i] == 'a'){
        a += 1;
    }
}

printf("%i", a);  // outputs: 3

```


##### Problem 4: How do we count the number of occurrences of all letters?

I am not going to spoil the fun here! 🤡

Here are some tips however:

- **Tip:** What I explained in Problem 1, 2, and 3 can be used here.

- **Tip:** My spider senses are tingling for some nested-loops?

- **Tip:** See how we looped through the alphabet earlier, perhaps that may come in handy!

- **Tip:** Use `tolower()` to make capitalized text more uniform.
