# Assignment_6_x
### Prerequisites
- `scanf()` / `printf()`
- Array
- Loop
- Functions
- String
- Structures


### Acknowledgments
- Thanks Mr. Norwegian Icecube 🧊 for pointing out my poor arithmetic!

# FAQ

The following chapters explain some neat tricks that may come in handy for all assignments within assignment 6.



## I don't understand Memory Management in C, is this normal?

**Yes. It is pretty difficult.** Don't fret if you don't understand it the first time around; most of you won't! 😵

I don't expect you to understand it fully after the course; I mean, I don't fully understand it myself!

But, if you do learn to understand Memory Management in C, I can promise you that you will have a better onset when learning other programming languages in the future, because memory management exist in all programming languages in some form. However, some languages exposes it to the user more than others (e.g. C vs Python).



## Memory Management Analogy

When you create a variable in your code, the variable must somehow be stored inside your computer's memory, that is, your computer's RAM. Very simplified, you can think of a computer's memory as a **storage room** containing many **storage boxes**. Each box is labeled with an **unique number** called an "**address**". Furthermore, just for the sake of making this example more spicy, assume that you suffer from amnesia. Due to hazy memory, you you have a notebook with you all the time containing notes about the whereabouts of each of your boxes in case you forget. 

**Rule:** When we want to **refer to a specific box**, we just refer it by **its corresponding address**.

![Your storage room](https://i.imgur.com/9EQKtQk.png)
This is your beautiful room of boxes!

### Using your storage room
When you declare a variable:

```c 
int a = 5; 
```

You can think of it as going to one of your boxes, e.g. the box with the address `8130`, write the value `5` on a slip of paper and putting it inside the box. You take your notebook and write down that variable `a` = `8130`.

We repeat the above steps with the code:
```c 
int b = 33; 
```
This time, the box with address `5124` is selected. You write the value `33` on a slip of paper before putting it inside the box, and add the line `b` = `5124` to your notebook.

The **current state** of our situation is the following:

|Notebook|Address|Content| 
|:------:|:-----:|:-----:|
|`a`	 |`8130` |`5`    |
|`b`	 |`5124` |`33`   |

When you see the following line of code:
```c 
int c = a + b; 
```

Given your severe case of amnesia, you use your notebook to check which two boxes `a` and `b` refer to. After learning that `a` refer to box `8130`, and `b` to box `5124`, you begin navigating to box `8130` to find the slip of paper with the value `5`. You do the same for box `5124`, finding the value `33`. Calculating your observations yields:

```c
int c = a + b;
int c = 5 + 33;
int c = 38;		// <-- Eureka!
```

You choose another box, e.g. the box labeled `3099`, and write the value `38` on a slip of paper before putting it inside. You note down in your notebook `c` = `3099`.

We end this part of our example with the following state:

|Notebook|Address|Content| 
|:------:|:-----:|:-----:|
|`a`	 |`8130` |`5`    |
|`b`	 |`5124` |`33`   |
|`c`	 |`3099` |`38`	 |



### Pointers analogy

As of now, each box are linked to one unique name. When we say `a`, we refer to box `8130`, and subsequently to value `5`. This raises an interesting question: ***Is it possible to have a box be referred by multiple names?***

**Note:** The above question implies that the content of the box can be altered using **any** of the names it is referred by.

Now, it could be tempting to say that the following line answers the question:
```c
int d = a;  // box '8130' is now referred by 'a' and 'd'?
```

But this is a **incorrect** assumption. Following the previous examples, we see that the above code just selects a new box (e.g. box `4452`), referred by `d`, with box `8130`'s value `5`, resulting the following state:

|Notebook|Address|Content| 
|:------:|:-----:|:-----:|
|`a`	 |`8130` |`5`    |
|`b`	 |`5124` |`33`   |
|`c`	 |`3099` |`38`	 |
|`d`	 |`4452` |`5`	 |

Furthermore, altering the value of `d` will **not** be reflected by `a`. Therefore, this assumption **does not** satisfy the question at hand.

That said, there exists a trick we can do to enable a box to be referred by multiple names. What if we instead assigned the value inside box `d` with the **address of `a`** as value? That is, the value `8130`, giving us the state:

|Notebook|Address|Content| 
|:------:|:-----:|:-----:|
|`a`	 |`8130` |`5`    |
|`b`	 |`5124` |`33`   |
|`c`	 |`3099` |`38`	 |
|`d`	 |`4452` |`8130` |


If we want to change the content of box `8130` to value `20`, we can do it in two different ways:

- Using `a`: 
	1. Use notebook and see `a` = `8130`
	2. Visit box `8130`
	3. Change content of its slip of paper to value `20`
	
- Using `d`:
	1. Use notebook and see `d` = `4452`
	2. Visit box `4452`
	3. Check inside the box and see the address `8130`
	4. Visit box `8130`
	5. Change content of its slip of paper to value `20`


**Two observations:**
 
1. Box `8130` can be referred by both `a` and `d`.
2. Although `d` refers to box `4452`, it also indirectly **points** towards box `8130`. 

**Conclusion:** `d` is a **pointer**!

## Pointers in code:

In short: **A pointer is an object that stores a memory address**.

Let's introduce **pointers** in more detail. The above examples can be coded like so:

##### The assignments before 'd' was introduced:

```c
int a = 5;
int b = 33;
int c = a + b;
```

Gives us the following state:

|Name	 |Address|Value  | 
|:------:|:-----:|:-----:|
|`a`	 |`8130` |`5`    |
|`b`	 |`5124` |`33`   |
|`c`	 |`3099` |`38`	 |

**Note:** Changed header `Notebook` to `Name`, and `Content` to `Value`, as this is more in-par with programming-lingo.



##### Declaration of pointer `d`:

The following lines declares a `int` **pointer** `d` with **random** value:

```c
int *d;  // Pointers are declared through the asterisk symbol (*)
```

|Name	 |Address|Value  | 
|:------:|:-----:|:-----:|
|`a`	 |`8130` |`5`    |
|`b`	 |`5124` |`33`   |
|`c`	 |`3099` |`38`	 |
|`d`	 |`4452` |???    |

As we have seen before with previous declarations, having unpredictable content inside our variables can lead to spooky cases! A way to circumvent this is to declare the pointer with the value `NULL`:

```c
int *d = NULL;
```

|Name	 |Address|Value  | 
|:------:|:-----:|:-----:|
|`a`	 |`8130` |`5`    |
|`b`	 |`5124` |`33`   |
|`c`	 |`3099` |`38`	 |
|`d`	 |`4452` |`0`    |

Setting a pointer's value to `NULL` is basically making the pointer point to a **placeholder** memory address. This is a great way to avoid spooky problems. In most cases, one should **always** assign a pointer to `NULL` if one is not sure where it should be pointing to.

Let us change the pointer `d`'s value to the **address** of `a`:

```c
int *d = NULL;
d = &a;	// The address of a variable is accessed by using ampersand symbol (&)
```

**IMPORTANT:** `&` is called the **reference-operator**

This moves us to the following state:

|Name	 |Address|Value  | 
|:------:|:-----:|:-----:|
|`a`	 |`8130` |`5`    |
|`b`	 |`5124` |`33`   |
|`c`	 |`3099` |`38`	 |
|`d`	 |`4452` |`8130` |


The above code can be shortened into one line:

```c
int *d = &a;
```

**Tip:** Note that given a **variable**, one can access **two things**: its **address** and its **value**.


### How does addresses look like?

You can try printing addresses:
```c
int a = 5;
    
printf("%p\n", &a);  // Prints: 000000000061FE14
printf("%i\n", &a);  // Prints: 6422036 
```

**Note:** The above addresses is from own computer. If you run the above code you will most likely get very different output.


## Dereferencing pointers

We have the following code:
```c
int n = 10;
int *p = &n; // 'p' points to 'n'
```


In the **storage room example** above, we managed to visit box `a` from box `d`. How do we do something similar with `int n` and `int *p`? 

If you print the above variables, you may get the following:

```c
printf("%i\n", n);  // Prints: 10
printf("%i\n", p);  // Prints: 6422032
```

We want to print `n`'s value through `p`, **but** the above code prints `n`'s address **instead** of its value. 

To "visit" the stored address in `p`, you must **dereference** `p` first:

```c
printf("%i\n", n);   // Prints: 10
printf("%i\n", *p);  // Prints: 10
```

**IMPORTANT:** `*` is called a **dereference operator**.

**Observation:** From my experiences, this is where **many of you will lose your foothold** when it comes to pointers. See below code snippet. 

```c
int n = 2 * 5; <-- The asterisk (*) here is multiplication

int *p = &n;   <-- The asterisk (*) here is for declaration of pointers, is NOT dereference operator!

int x = *p;    <-- The asterisk (*) here is dereference operator, NOT the same as the one above!
```

And to make it worse, this is possible 😱: 

```c
int n = 5;

int *p1 = &n;	 // Pointer that points to integer!

int **p2 = &p1;  // Pointer that points to pointer that points to integer!

// Dereferencing p1
printf("%i\n", *p1);    // Outputs: 5

// Double-dereferencing p2
printf("%i\n", **p2);   // Outputs: 5
```

**CONCLUSION**: The **dereferencing operator** `*` is basically the **opposite operation** of the **reference operator** `&`.

Yeah, pointers are very **oof** sometimes. It do be like that sometimes. 🤯



## What is the deal with `sizeof()`?

`sizeof()` is a function you can use to determine the **size** of a variable. The size is in **number of bytes**.

|Type			|Storage size	|
|---------------|---------------|
|**char**		|**1 byte** 	|
|unsigned char	|1 byte 		|
|signed char	|1 byte 		|	
|**int**		|**4 bytes**	|
|unsigned int	|4 bytes		|
|short			|2 bytes		|
|unsigned short	|2 bytes		|
|**float**		|4 bytes		|
|**long**		|**8 bytes**	|
|unsigned long	|8 bytes		|

**Tip:** Bolded types are the ones you probably uses the most when programming in C.

**Note:** The storage sizes above may vary from computers to computers depending on the used hardware. Most likely, your computer has identical specs as summarized here.

```c
int a = 5;
printf("%i", sizeof(a)); // Outputs: 4
```

### What about `sizeof()` and arrays?

**Observation:** I remember seeing very many students believing that using `sizeof()` on an array outputs the **length of the array** (similar to how `len()` works in python). This is **incorrect**!

This misunderstanding is easily dispelled:
```c
int my_array[20] = {0};
printf("%i", sizeof(my_array)); // Outputs: 80
```

As you can see above, `sizeof()` returns `80` and not `20`. `sizeof()` calculates the **number of bytes** this array uses in the memory. Our array `my_array` contains 20 integers and, naturally, `sizeof()` returns `20 * 4 = 80`, where **4 byte** is the size of **one** `int`.

### What about `sizeof()` and structs?

Luckily for us, using `sizeof()` with structs is very intuitive. See below:

```c
struct Student{
	int age;		// 1 int   = 4 bytes
	char name[20];	// 20 char = 20 bytes
	char id[20];	// 20 char = 20 bytes
};

printf("%i", sizeof(struct Student));  // Outputs: 44 (because 44 = 4 + 20 + 20) 
```

Quick maths! 😎

## What is `malloc()`?

`malloc()` stands for **Memory Allocation**. As the name implies, `malloc()` is used when you want to **allocate some memory** to a variable.

```c
int *p = malloc(4); // Allocate 4 bytes to pointer 'p'
*p = 5;				// Assign value 5 to referenced memory.
``` 

**Tip:** You can use `sizeof()` with `malloc()`!

```c
int *p = malloc(sizeof(int)); 	// Allocate sizeof(int) = 4 bytes to pointer 'p'
*p = 5;							// Assign value 5 to referenced memory.
```

**Note:** `malloc()` allocates memory from the **heap**. I won't be going into detail the difference between **heap** and **stack** as I don't know the difference well enough to explain well!

### What happens if you `malloc()` more bytes?

We know that if you `malloc(24)` to your variable, your variable will have enough space to store **24 bytes of data**. Recall that one `int` = 4 bytes in size. This means that **24 bytes can fit 6 `int`**. You know what else fits 6 `int`? An `int` array!

In the below code, both variables `arr_0` and `arr_1` **are allocated 24 bytes each**. 

```c
int arr_0[6];

int arr_1 = malloc(6 * sizeof(int));
```

**However**, `arr_0` has allocated bytes from the **stack**, while `arr_1` from the **heap**.


## What is `realloc()`?

`realloc()` stands for **Reallocation of Memory**. As the name implies, `realloc()` is used when you want to **resize the already allocated memory**.

```c
int numbers = malloc(10 * sizeof(int)); // 'numbers' has enough space for 10 integers


numbers = realloc(numbers, 20 * sizeof(int)); // 'numbers' has enough space for 20 integers
```


## What is `free()`

When you **allocate something from the heap**, that is, when you use `malloc()`, you should `free()` that allocation when done. 

**IMPORTANT:** Freeing some memory means **making said memory available** for future allocations.

```c
int *p = malloc(sizeof(int));
*p = 5;

printf("%i", *p);

free(p); // De-allocate the memory `p` uses.
```


**Observation:** I have seen multiple students think `free()` deletes the value of a pointer. This is **wrong**! `free()` just makes the allocated memory space available for future `malloc()`. 

The following two chapters are about best practices when using `free()`.

### Avoid memory leak!
 
Using `free()` is all about **correct timing**. Consider the following code:

```c
int *p = malloc(sizeof(int));	// 1st malloc: Allocates 4 bytes to 'p'
*p = 5;
printf("%i\n", *p);	// Outputs: 5


p = malloc(sizeof(int));		// 2nd malloc: Allocates 4 NEW bytes to 'p'
*p = 6;
printf("%i\n", *p);	// Outputs: 6

free(p);  // De-allocates the 4 bytes allocated from the 2nd malloc()
```

Did you spot a **problem** in the above code snippet? We forgot to `free(p)` before second `malloc()`! The **allocated memory** after first `malloc()` won't be available for future allocations anymore! This is phenomenon is called **memory leak**: *Your code is leaking usable memory*. Too much **memory leak** may crash the execution.

Don't worry, when you **terminate the execution**, all allocated memory will be automatically **freed**.

However, you should avoid memory leak whenever you can. For the above code, consider this fix:

```c
int *p = malloc(sizeof(int));	// 1st malloc: Allocates 4 bytes to 'p'
*p = 5;
printf("%i\n", *p);	// Outputs: 5

free(p);  // <-- This line cancels the memory leak!


p = malloc(sizeof(int));		// 2nd malloc: Allocates 4 NEW bytes to 'p'
*p = 6;
printf("%i\n", *p);	// Outputs: 6

free(p);  // De-allocates the 4 bytes allocated from the 2nd malloc()
```




### Avoid dangling pointers!

After de-allocating a pointer with `free()`, it is recommended to set said pointer to point to `NULL`, as seen below:

```c
int *p = malloc(sizeof(int));
*p = 5;

printf("%i", *p);

free(p); // De-allocate the memory `p` uses.

p = NULL;  // <-- Removes a dangling pointer!
```

As mentioned above, `free()` does **not** delete the value stored in the pointer; it still points to the **same** memory address! Consider the following oopsie-daisy:

```c
int *p = malloc(sizeof(int));
*p = 5;

printf("%i", *p);

free(p); // De-allocate the memory `p` uses.

*p = 10; // Oh no! Assigning values to de-allocated address!
```

Although the code above won't necessarily crash, it may encounter problems in the future. As the memory address is de-allocated, it may be allocated to a different pointer in the future. The last line of code above may alter some values it should not have access to!

```c
int *p = malloc(sizeof(int));
*p = 5;

printf("%i", *p);

free(p); // De-allocate the memory `p` uses.

p = NULL;  // <-- Removes a dangling pointer!

*p = 10; // No worries, this code won't mess with any other pointer anymore!
```