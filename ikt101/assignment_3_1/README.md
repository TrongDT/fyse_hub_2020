# Assignment_3_1
### Prerequisites
- `scanf()` / `printf()`
- Array
- Loop
- Functions

## Steps

### Step 1 - Create an array that can hold **10 integers**
```c
int numbers[10] = {0};
```
**Tip**: Initialize list with numbers.
```c
int numbers[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
```

**Tip**: Create a loop that prints all numbers for easier debugging.

```c
// Print all 10 numbers
for (int i = 0; i < 10; ++i) {
    printf("%i ", numbers[i]);
}
```

### Step 2 - Begin creating a loop that takes in 10 integers
```c
for (int i = 0; i < 10; ++i) {
    scanf("%i", &numbers[i]);
}
```

#### Current state
Define your functions above `main()`:
```c
#include <stdio.h>

// TODO: DEFINE YOUR FUNCTIONS HERE

int main() {
    // Write a program that reads in 10 integers
    int numbers[10] = {0,1,2,3,4,5,6,7,8,9};  

    // Scan in 10 numbers
    for (int i = 0; i < 10; ++i) {
        scanf("%i", &numbers[i]);
    }
    
    // Print all 10 numbers
    for (int i = 0; i < 10; ++i) {
        printf("%i ", numbers[i]);
    }

    // TODO: CALL YOUR FUNCTIONS HERE    

    return 0;
}

```
### Step 3 - Create functions
Our functions will take in two inputs (aka. **parameters**):
1. An integer array `int arr[]`
2. An integer `int len` that represents the length of the array

**Note: ** The parameter names `arr` and `len` are both just **arbritrary** names we use in our example. You can rename them 😀!

** IMPORTANT: ** 
The functions **must** be called:
- `minimum` with return type `int`
- `maximum` with return type `int`
- `sum` with return type `int`
- `average` with return type `float`
- `median` with return type `float`

#### Step 3.1 - Summation Function:
```c
int sum(int arr[], int len){
    int s = 0;
    for (int i = 0; i < len; ++i) {
    	s += arr[i]; // Add current value to existing sum
    }
    return s;
}
```
	
#### Step 3.2 - Average Function:
Rembember to make the `average` return `float`. Try finishing this one yourself!
**Tip:** You can **call** your existing functions inside other functions!
```c
float average(int arr[], int len){
    int avg = sum(???, ???) / ???;
    return avg;
}
```

#### Step 3.3 - Minimum Function:
Two good solutions here:
###### Version 1 - Begin `smallest` as a huge number
```c
int minimum(int arr[], int len){
    int smallest = 99999; // Store a very high number here!
    for (int i = 0; i < len; ++i) {
    	if(smallest > arr[i]){
    		smallest = arr[i];
    	}
    }
    return smallest;
}
```


###### Version 2 - Begin `smallest` as the first number in our array
```c
int minimum(int arr[], int len){
    int smallest = arr[0]; // <-- Store first number
    for (int i = 1; i < len; ++i) { // index begins at 1
    	if(smallest > arr[i]){
    		smallest = arr[i];
    	}
    }
    return smallest;
}
```

#### Step 3.4 - Maximum Function:
**Tip:** Use the structure of `minimum()` as inspiration.
```c
int maximum(int arr[], int len){
    ...
}
```

#### Step 3.5 - Median Function:
```c
// PSEUDOCODE
int median(int arr[], int len){
    // 1. Sort array in ascending order
    // 2. Get the average of the two middle numbers
    // 3. Return the result from step 2
}
```

Before you calculate the **median**, you should **sort** your array such that the numbers are in ascending order (e.g. 1, 5, 7, ...). This makes it easier!

Two reasonable ways to sort your array: Either you create your own function that sorts (e.g. [Bubble Sort](https://en.wikipedia.org/wiki/Bubble_sort)), or you use [`qsort()`](https://www.tutorialspoint.com/c_standard_library/c_function_qsort.htm) from the library `<stdlib.h>`.

We'll only cover the former, that is, the (in)famous bubble sort!

###### Example: Bubble sort of 5 numbers:

`5 1 4 2 8` For our example, we will work with these 5 numbers.
Bubble sort is all about **pairwise comparisons**. We denote pairs with brackets (`[ ]`).
`[5 1] 4 2 8` **1st pair**: Is **left number** higher than **right number** (i.e. `5 > 1`)?
1. `[5 1] 4 2 8` -> `[1 5] 4 2 8` Repeat until you reach the end: If `left` > `right` => `swap`
1. `1 [5 4] 2 8` -> `1 [4 5] 2 8` 
1. `1 4 [5 2] 8` -> `1 4 [2 5] 8` 
1. `1 4 2 [5 8]` -> `1 4 2 [5 8]` Don't swap here, as left is less than right.
1. Result: `1 4 2 5 8`  

We are not complete: We only moved highest number from left to right-most position.
Going from **left-most pair** to **right-most pair** is **one iteration** of our bubble sort. If you repeat the above steps 5 times, your list of numbers will be correctly sorted!

###### Bubble Sort in C
The code for Bubble Sort is a bit intimidating, as you will have a **nested-loop**; a loop inside another loop! 

**Step 1 - Create function with nested-loop**
```c
void bubble_sort(int arr[], int len){
    // Loops for 'len' times (where len = number of integers)
    for (int i = 0; i < len; ++i) {
        // Loop for 'len' times
        for (int j = 0; j < len; ++j) {
			...
        }
    }
}
```
**Step 2 - Add comparison**
```c
void bubble_sort(int arr[], int len){
    for (int i = 0; i < len; ++i) {
        for (int j = 0; j < len; ++j) {
            // Check if left number is higher than right number
            if(arr[j] > arr[j + 1]){
	            // If True => Perform swap
            }
        }
    }
}
```
**Step 3 - Complete!**
```c
void bubble_sort(int arr[], int len){
    for (int i = 0; i < len; ++i) {
        for (int j = 0; j < len; ++j) {
            if(arr[j] > arr[j + 1]){
                // Swap
                int backup = arr[j];  // Backup value inside arr[j]!
                arr[j] = arr[j + 1];  // Overwrite arr[j] with arr[j+1]
                arr[j + 1] = backup;  // Overwrite arr[j+1] with backup
            }
        }
    }
}
```
Let us go back to our `median()` function.
#### Median Function
```c
void bubble_sort(int arr[], int len){
    // The aforementioned code!
}

int median(int arr[], int len){
    // 1. Sort array in ascending order
    bubble_sort(arr, len); // No return value, 'arr' is sorted after this line.
    
    // 2. Get the average of the two middle numbers
    ???
    
    // 3. Return the result from step 2
    return ???
}
```
Good luck!
